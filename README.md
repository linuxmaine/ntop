 # ntop

## contexte

Ce dépôt montre comment mesurer le volume de données nécessaire lors de l'installation de windows et comment le comparer à une isntallation linux

## Schéma

```mermaid
flowchart TB

internet(("internet"))

subgraph PROXMOX

    nodeNavigateur("navigateur")
    subgraph VMs
    
    nodeWindows("windows<br>192.168.42.101")
    nodeLinux("Linux<br>192.168.42.100")

    nodeLinux --- nodeintEth0
    nodeWindows --- nodeintEth0

    subgraph "Ntop"

    nodeintEth0("enp0s8<br>192.168.42.1")
    nodeintEth1("enp0s3"<br>192.168.1.100)
    
    nodeintEth0 ---|ipForwarding| nodeintEth1

    end
    
    end
    nodeintEth1 ---|3000| nodeNavigateur

end

BOX["BOX<br>192.168.1.254"]

nodeintEth1 --- BOX
BOX --- internet

```


## configuration

### Création d'un template debian11

Téléchargement de l'image

wget http://cloud.debian.org/images/cloud/bullseye/latest/debian-11-genericcloud-amd64.qcow2

Création d'une VM

qm create 99003 --name debian11-cloudinit-template --memory 2048 --net0 virtio,bridge=vmbr0 --scsihw virtio-scsi-pci

Import de l'image disque

qm disk import 99003 debian-11-genericcloud-amd64.qcow2 local-lvm

Ajout d'un disque cloud-init

qm set 99003 --ide1 local-lvm:cloudinit

Configuration du port série 

qm set 99003 --serial0 socket --vga serial0

Activation du disque

qm set 99003 --scsi0 local-lvm:vm-99003-disk-0

?

qm set 99003 --agent enabled=1

Modification de l'ordre de démarrage

qm set 99003 --boot order="scsi0;net0"

Création du template

qm template 99003

### Création de la VM

qm clone 99003 100 --description "vm-debian11" --full 1 --name "vm-debian11" --pool pooldebian

qm set 100 \
--memory 4096 \
--sockets 1 --cores 4 \
-ciuser starxpert -cipassword starxpert \
-ciupgrade 0 \
-ipconfig0 ip=192.168.1.217/24,gw=192.168.1.254 \
-sshkeys ~/.ssh/id_rsa.pve.pub \
--searchdomain valog.fr --nameserver 192.168.1.99
  
qm resize 100 scsi0 +8G
qm start 100
